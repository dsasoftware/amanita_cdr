import logging
from odoo import models, fields


logger = logging.getLogger(__name__)


class CdrServer(models.Model):
    _inherit = 'amanita_base.server'
    
    asterisk_keep_recordings = fields.Boolean(string='Keep Recordings',
        help='Keep recordings on Asterisk after uploading to Odoo.')

