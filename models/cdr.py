from datetime import datetime, timedelta
import pytz
import logging
from odoo import models, fields, api, _
from nameko.exceptions import RemoteError, RpcTimeout

logger = logging.getLogger(__name__)

DISPOSITION_TYPES = (
    ('NO ANSWER', 'No answer'),
    ('FAILED', 'Failed'),
    ('BUSY', 'Busy'),
    ('ANSWERED', 'Answered'),
    ('CONGESTION', 'Congestion'),
)

DEFAULT_CDR_KEEP_DAYS = 180


class Cdr(models.Model):
    _name = 'amanita_cdr.cdr'
    _description = 'Call Detail Record'
    _order = 'started desc'
    _rec_name = 'uniqueid'

    server = fields.Many2one('amanita_base.server')
    accountcode = fields.Char(size=20, string='Account code', index=True)
    src = fields.Char(size=80, string='Src', index=True)
    dst = fields.Char(size=80, string='Dst', index=True)
    dcontext = fields.Char(size=80, string='Dcontext')
    clid = fields.Char(size=80, string='Clid', index=True)
    channel = fields.Char(size=80, string='Channel', index=True)
    dstchannel = fields.Char(size=80, string='Dst channel', index=True)
    lastapp = fields.Char(size=80, string='Last app')
    lastdata = fields.Char(size=80, string='Last data')
    started = fields.Datetime(index=True, oldname='start')
    answered = fields.Datetime(index=True, oldname='answer')
    ended = fields.Datetime(index=True, oldname='end')
    duration = fields.Integer(string='Duration', index=True)
    billsec = fields.Integer(string='Billsec', index=True)
    disposition = fields.Char(size=45, string='Disposition', index=True)
    amaflags = fields.Char(size=20, string='AMA flags')
    userfield = fields.Char(size=255, string='Userfield')
    uniqueid = fields.Char(size=150, string='Uniqueid', index=True)
    peeraccount = fields.Char(size=80, string='Peer account', index=True)
    linkedid = fields.Char(size=150, string='Linked id')
    sequence = fields.Integer(string='Sequence')
    recording_available = fields.Boolean(index=True)
    recording_filename = fields.Char()
    recording_data = fields.Binary()
    recording_attachment = fields.Binary(attachment=True, string=_('Download'))
    recording_widget = fields.Char(compute='_get_recording_widget')
    recording_icon = fields.Char(compute='_get_recording_icon')
    # QoS
    ssrc = fields.Char(string='Our SSRC')
    themssrc = fields.Char(string='Other SSRC')
    lp = fields.Integer(string='Local Lost Packets')
    rlp = fields.Integer(string='Remote Lost Packets')
    rxjitter = fields.Float(string='RX Jitter')
    txjitter = fields.Float(string='TX Jitter')
    rxcount = fields.Integer(string='RX Count')
    txcount = fields.Integer(string='TX Count')
    rtt = fields.Float(string='Round Trip Time')
    # CEL related fields
    cel_count = fields.Integer(compute='_get_cel_count')
    cels = fields.One2many(comodel_name='amanita_cdr.cel',
                           inverse_name='cdr')

    @api.model
    def create(self, vals):
        # Update time acctording to creating user tz
        if self.env.user.tz:
            try:
                server_tz = pytz.timezone(self.env.user.tz)
                convert_fields = ['started', 'answered', 'ended']
                for field in convert_fields:
                    if field in vals and vals[field]:
                        dt_no_tz = fields.Datetime.from_string(vals[field])
                        dt_server_tz = server_tz.localize(dt_no_tz,
                                                          is_dst=None)
                        dt_utc = dt_server_tz.astimezone(pytz.utc)
                        vals[field] = fields.Datetime.to_string(dt_utc)
            except Exception:
                logger.exception('Error adjusting timezone for CDR')
        return super(Cdr, self).create(vals)


    @api.model
    def create_cdr(self, event):
        get = event['headers'].get
        # Get the server by systen name
        system_name = event['headers'].get('SystemName')
        server = self.env['amanita_base.server'].search(
                                        [('system_name', '=', system_name)])
        if not server:
            logger.error('Server not found by system name %s!', system_name)
        data = {
            'accountcode': get('AccountCode'),
            'src': get('Source'),
            'dst': get('Destination'),
            'dcontext': get('DestinationContext'),
            'clid': get('CallerID'),
            'channel': get('Channel'),
            'dstchannel': get('DestinationChannel'),
            'lastapp': get('LastApplication'),
            'lastdata': get('LastData'),
            'started': get('StartTime') or False,
            'answered': get('AnswerTime') or False,
            'ended': get('EndTime') or False,
            'duration': get('Duration'),
            'billsec': get('BillableSeconds'),
            'disposition': get('Disposition'),
            'amaflags': get('AMAFlags'),
            'uniqueid': get('UniqueID') or get('Uniqueid'),
            'linkedid': get('Linkedid'),
            'userfield': get('UserField'),
            'server': server.id if server else False,
        }
        self.create(data)
        return True

    @api.multi
    def _get_cel_count(self):
        for rec in self:
            rec.cel_count = self.env['amanita_cdr.cel'].search_count([
                ('cdr', '=', rec.id)])


    @api.multi
    def _get_recording_icon(self):
        for rec in self:
            if rec.recording_available:
                rec.recording_icon = '<span class="fa fa-file-sound-o"/>'
            else:
                rec.recording_icon = ''


    @api.multi
    def _get_recording_widget(self):
        recording_storage = self.env['res.config.settings'].sudo(
                            )._get_amanita_param('recording_storage')
        if recording_storage == 'db':
            recording_source = 'recording_data'
        else:
            recording_source = 'recording_attachment'
        for rec in self:
            rec.recording_widget = '<audio id="sound_file" preload="auto" ' \
                    'controls="controls"> ' \
                    '<source src="/web/content?model=amanita_cdr.cdr&' \
                    'id={}&filename={}&field={}&' \
                    'filename_field=recording_filename&download=True" ' \
                    'type="audio/wav"/>'.format(rec.id, rec.recording_filename,
                                                recording_source)


    @api.model
    def update_qos(self, values):
        if type(values) == list:
            values = values[0]
        uniqueid = values.get('uniqueid')
        linkedid = values.get('linkedid')
        # TODO Probably we need to optimize db query on millions of records.
        cdrs = self.env['amanita_cdr.cdr'].search([
            #('ended', '>', (datetime.now() - timedelta(seconds=120)).strftime(
            #    '%Y-%m-%d %H:%M:%S')
            #),
            ('uniqueid', '=', uniqueid),
            #('linkedid', '=', linkedid),
        ])
        if not cdrs:
            logger.info('Omitting QoS, CDR not found, uniqueid {}!'.format(
                uniqueid))
            return False
        else:
            logger.debug('Found CDR for QoS.')
            cdr = cdrs[0]
            cdr.write({
                 'ssrc': values.get('ssrc'),
                 'themssrc': values.get('themssrc'),
                 'lp': int(values.get('lp')),
                 'rlp': int(values.get('rlp')),
                 'rxjitter': float(values.get('rxjitter')),
                 'txjitter': float(values.get('txjitter')),
                 'rxcount': int(values.get('rxcount')),
                 'txcount': int(values.get('txcount')),
                 'rtt': float(values.get('rtt'))
            })
            return True


    @api.model
    def get_call_recording(self, event):
        # This method called by Odoo broker after delay so CDR must be already here.
        if event['headers']['Event'] != 'Hangup':
            logger.error(
                'Wrong event passed to upload_recording_on_hangup: %s', event)
            return False
        cdr = self.env['amanita_cdr.cdr'].search([
                            ('uniqueid', '=', event['headers']['Uniqueid'])])
        if not cdr:
            logger.info('CDR not found for recording upload, uniquid %s',
                        event['headers']['Uniqueid'])
            return False
        # TODO: Sometimes we have 2 CDRs with same uniqueid. Hello Asterisk!
        cdr = cdr[0]
        if not cdr.server:
            logger.info('CDR id %s does not have server defined!', cdr.id)
            return False            
        # Get recording from the cache
        file_path = self.env['amanita_base.cache'].get(
            event['headers']['Uniqueid'], family='MIXMONITOR_FILENAME')
        if not file_path:
            return False
        # We have recording for the channel. Get recording
        with cdr.server.get_server_proxy('fileman') as proxy:
            try:
                res = proxy.get_asterisk_call_recording(file_path,
                    keep=cdr.server.asterisk_keep_recordings)
            except (RemoteError, RpcTimeout) as e:
                logger.error('Could not get recording: %s', e)
                return False
            if not res:
                logger.error('Could not get_asterisk_recording %s',
                             file_path)
                return False
            res['uniqueid'] = event['headers']['Uniqueid']
            return self.save_call_recording(cdr.id, res)

    @api.model
    def save_call_recording(self, cdr_id, values):
        storage = self.env['res.config.settings'].sudo(
                                    )._get_amanita_param('recording_storage')
        if storage == 'db':
            recording_field = 'recording_data'
        else:
            recording_field = 'recording_attachment'
        cdr = self.browse(cdr_id)
        cdr.write({
            'recording_available': True,
            'recording_filename': '{}.wav'.format(values['uniqueid']),
            recording_field: values['data'],
        })
        return True


    @api.model
    def vacuum(self, days=DEFAULT_CDR_KEEP_DAYS):
        BULK_SIZE = 1000  # Unlink by blocks of 1000 records
        while True:
            try:
                records = self.env['amanita_cdr.cdr'].search(
                    [('started', '<', fields.Datetime.to_string(
                     datetime.now() - timedelta(days=days)))], limit=BULK_SIZE)
                if records:
                    logger.info('Deleting {} CDRs.'.format(len(records)))
                    records.unlink()
                    self.env.cr.commit()
                else:
                    break
            except Exception as e:
                logger.info('CDR vacuum error: %s', str(e))
                break


