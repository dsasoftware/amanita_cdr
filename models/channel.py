import logging
import time
from odoo import models, fields, api


logger = logging.getLogger(__name__)


class CdrChannel(models.Model):
    _inherit = 'amanita_base.channel'

    # We catch VarSet with MIXMONITOR_FILENAME Variable and keep it here
    monitor_filename = fields.Char()

    @api.model
    def update_monitor_filename(self, event):
        if event['headers'].get('Variable') == 'MIXMONITOR_FILENAME':
            file_path = event['headers']['Value']
            uniqueid = event['headers']['Uniqueid']
            linkedid = event['headers']['Linkedid']
            channels = self.search(
                ['|', ('uniqueid', '=', uniqueid),
                      ('linkedid', '=', linkedid)])
            for chan in channels:
                self.env['amanita_base.cache'].put(uniqueid, file_path,
                                                family='MIXMONITOR_FILENAME')
            return True
        else:
            # We return False meaning we did not use VarSet. Byt who cares? :-P
            return False

