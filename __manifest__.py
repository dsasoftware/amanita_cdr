{
    'name': 'Amanita Telefonica Call Detail Records',
    'summary': '',
    'description': """Call detail records addon""",
    'version': '12.0.1.0',
    'category': 'Phone',
    'author': 'Odooist',
    'license': 'LGPL-3',
    'depends': ['amanita_base'],
    'installable': True,
    'application': False,
    'auto_install': False,
    'data': [
        'security/amanita_admin/ir.model.access.csv',
        'security/amanita_agent/ir.model.access.csv',
        'security/amanita_report/ir.model.access.csv',
        'security/amanita_user/ir.model.access.csv',
        'views/cdr.xml',
        'views/cel.xml',
        'views/server.xml',
        # Data
        'data/events.xml',
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'images': ['static/description/amanita_cdr.png'],
}
